@section('title')
Fonda Cony: Inicia Sesion
@endsection

@extends('layouts.app')

@section('content')




<div class="page-header header-filter" style="background-image: url('{{ asset('img/login.jpg') }}'); background-size: cover; background-position: top center;">
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
            <div class="card card-signup ">
                <form class="form" method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="header header-danger text-center">
                        <h4 class="card-title">Iniciar sesion</h4>
                    </div>
                    <p class="description text-center">Ingresa tus datos</p>
                    <div class="card-content">

                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">email</i>
                            </span>
                            
                            <div class="form-group is-empty">

                               <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="E-mail">

                                @if ($errors->has('email'))
                                    <span class="text-danger">
                                        {{ $errors->first('email') }}
                                    </span>
                                @endif

                            </div>

                        </div>

                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">lock_outline</i>
                            </span>
                            <div class="form-group is-empty">


                               <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Contraseña">

                                @if ($errors->has('password'))
                                    <span class="text-danger">
                                        {{ $errors->first('password') }}
                                    </span>
                                @endif

                            </div>
                        </div>

                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Recordarme
                                    </label>
                        </div>

                    </div>
                    <div class="footer text-center">
                        <button type="submit" class="btn btn-danger btn-wd btn-lg">
                                    Entrar
                        </button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

    </div>



@endsection
