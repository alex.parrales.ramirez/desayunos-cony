@section('title')
Dasayunos Cony: Administrar sitio
@endsection

@extends('layouts.app')

@section('content')
<body>
	
	<div class="page-header header-filter header-small" data-parallax="true" style="background-image: url('{{ asset('img/cover.jpg') }}');">
    <div class="container">
      <div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="brand">
						<h1 class="title text-center">Administrar sitio
              
            </h1>
						
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="main main-raised">
		<div class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-4">

						<div class="card card-background" style="background-image: url('assets/img/examples/office1.jpg')">

							<div class="card-content content-success">
								<a href="#pablo">
									<h3 class="card-title">Administrar pedidos</h3>
								</a>
								<p class="card-description">
									Cambie el estatus de los pedidos
								</p>
								<a href="{{ route('carts.index') }}" class="btn btn-danger btn-round">
									<i class="material-icons">subject</i> Administrar
								</a>
							</div>
						</div>
					</div>
					<div class="col-md-4">

						<div class="card card-background" style="background-image: url('assets/img/examples/office1.jpg')">

							<div class="card-content content-danger">
								<a href="#pablo">
									<h3 class="card-title">Administrar comidas</h3>
								</a>
								<p class="card-description">
									Cambie el estatus de las comidas
								</p>
								<a href="{{ route('foods.index') }}" class="btn btn-danger btn-round">
									<i class="material-icons">subject</i> Administrar
								</a>
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="card card-background" style="background-image: url('assets/img/examples/office1.jpg')">

							<div class="card-content content-info">
								<a href="#pablo">
									<h3 class="card-title">Administrar usuarios</h3>
								</a>
								<p class="card-description">
									Cambie el estatus de los usuarios
								</p>
								<a href="{{ route('users.index') }}" class="btn btn-danger btn-round">
									<i class="material-icons">subject</i> Administrar
								</a>
							</div>
						</div>
					</div>

	    				</div>
						</div>
					</div>
				</div>





</body>